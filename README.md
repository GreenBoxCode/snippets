# snippets
## Compiler instructions

### C  
```gcc -Wall -std=c2x -Os test.c -o test```

### C++
```gcc -Wall -std=c++0x -Os main.cpp -o main```

