// gcc -Wall -std=c2x -Os circle_area.c -o circa
#include <stdlib.h>
#include <stdio.h>

void circle_area(const long double *Pi, long double *radius, long double *area);

int main (int argc, char *argv[]) {
  const long double Pi = 3.1415926535897932384626433832795029L;
  if (argc > 2 || argc == 1){ 
    printf("Please enter a decimal radius\n");
    return 1;
    }
  long double radius = strtold(argv[1],NULL);
  long double area = 0.0L;
  circle_area(&Pi, &radius, &area);
  if (area > 1){
    printf("Area of the circle is:\t%Lf\n", area);
  }
  else {
    printf("Err: Positive numeric values only.");
    return 1;
  }

  return 0;
}

void circle_area(const long double *Pi, long double *radius, long double *area) {
  *area = *Pi * *radius * *radius;
}
