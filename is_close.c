#include <stdio.h>
#include <stdlib.h>

#define TRUE 1
#define FALSE 0

long double epsilon =0.0L;
long double x = 0.0L;
long double y = 0.0L;

int is_close(long double *x, long double *y, long double *epsilon);

int main(int argc, char **argv) {
  if (argv[1] != NULL) {
    x = strtold(argv[1], NULL);
    y = strtold(argv[2], NULL);
    epsilon = strtold(argv[3], NULL);
  } else {
    printf("Please enter at least one variable to compare for equality\n");
  }

  if (is_close(&x, &y, &epsilon)) {
    printf("true\n");
    return 0;
  } else {
    printf("false\n");
    return 0;
  }
  return 0;
}

int is_close(long double *x, long double *y, long double *epsilon) {
  long double delta = *x - *y;
  if (delta < 0) {
    delta *= -1.0L;
  }
  if (delta > *epsilon) {
    return FALSE;
  }
  return TRUE;
}
